$(function() {
    //从Layui中获取form对象
    var form = layui.form;
    var layer = layui.layer;
    //通过form.verify()函数自定义校验规则
    form.verify({
        //自定义了一个叫做pwd校验规则
        // pwd: [/^[\S]{6,12}$/, '密码必须6到12位，且不能出现空格'],
        pwd: [/^admin888$/, "密码输入错误，请重试"],
    });

    //监听登录表单的提交事件
    $("#form-login").submit(function(e) {
        //阻止默认提交行为
        e.preventDefault();
        $.ajax({
            url: "http://122.51.249.55:8083/api/login",
            method: "POST",
            //快速获取表单中的数据
            data: $(this).serialize(),
            success: function(res) {
                if (res.code !== 200) {
                    return layer.msg("登录失败！");
                }
                layer.msg("登陆成功！");
                // console.log(res.token);
                // 将登陆成功得到的字符串保存到localStorrage中
                localStorage.setItem("token", res.token);
                //跳转到后台主页
                location.href = "index.html";
            },
        });
    });
});
// $(function() {
//     $('.message').on('mouseenter', function() {
//         alert('ok')
//     })
// })