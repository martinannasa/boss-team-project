$(function() {

    var layer = layui.layer
    var form = layui.form
    var laypage = layui.laypage
    var laydate = layui.laydate
        // 定义美化时间过滤器
    template.defaults.imports.dateFormat = function(time) {
            return (new Date(parseInt(time))).toLocaleDateString()
        }
        // 定义查询参数对象q
    var q = {
        pagenum: 1, // 页码值，默认请求第一页的数据
        pagesize: 5, // 每页显示几条数据，默认每页显示2条
        act: 'getlist', //getlist表示获取列表，find表示查找某个文章
        id: '',
        keywords: '',
        catename: '',
        createtime: ''
    }
    laydate.render({
        elem: '#test1', //指定元素
        // range: true
    });
    // 初始化表格
    initArticleList()
        //获取文章列表
    function initArticleList() {
        $.ajax({
            type: "GET",
            url: "http://122.51.249.55:8083/api/article",
            data: q,
            success: function(res) {
                // console.log(res);
                if (res.code !== 200) {
                    return layer.message('获取文章列表失败！')
                }
                var htmlStr = template("tpl-table", res)
                $("tbody").html(htmlStr)
                    // 渲染分页区域
                renderPage(res.allNum)
            }
        });
    }

    // 渲染筛选区域下拉选择框
    initCate()

    function initCate() {
        $.ajax({
            type: "GET",
            url: "http://122.51.249.55:8083/api/article",
            data: q,
            success: function(res) {
                console.log(res)

                var htmlStr = template("tpl-cate", res)
                $("[name=catename]").html(htmlStr)
                    // console.log(htmlStr)
                form.render();
            }
        });
    }
    // 根据筛选条件显示列表区域内容
    $("#form-search").on("submit", function(e) {
        e.preventDefault()
        var catename = $("[name=catename]").val()
        var keywords = $("[name=keywords]").val()
        var createtime = new Date().getTime($('.shijian').val())
        console.log(createtime);
        q.catename = catename
        q.keywords = keywords
        q.createtime = createtime
        initArticleList()
    })
    layui.use('laypage', function() {})
        // 渲染分页
    function renderPage(allNum) {
        // console.log(total);
        laypage.render({
            elem: 'pageBox',
            count: allNum, //数据总数，从服务端得到
            limit: q.pagesize,
            curr: q.start,
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [2, 3, 5, 10],
            // 触发 jump 回调的方式有两种：
            // 1. 点击页码的时候，会触发 jump 回调
            // 2. 只要调用了 laypage.render() 方法，就会触发 jump 回调(这是首次触发)
            jump: function(obj, first) {
                //obj包含了当前分页的所有参数，比如：
                // console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                // console.log(obj.limit); //得到每页显示的条数
                q.pagesize = obj.limit
                q.start = obj.curr
                    //首次不执行
                    // console.log(first);
                if (!first) {
                    // initArticleList()
                    initArticleList()
                        // console.log(11111);
                }
            }
        });
    }
    //删除
    // 通过代理的形式，为删除按钮绑定点击事件处理函数
    $('tbody').on('click', '.btn-delete', function() {
        // 获取删除按钮的个数
        var len = $('.btn-delete').length
        console.log(len)
            // 获取到文章的 id
        var id = $(this).attr('data-id')
            // 询问用户是否要删除数据
        q.id = id;
        q.act = 'del'
        console.log(q);
        layer.confirm('确认删除?', { icon: 3, title: '提示' }, function(index) {
            $.ajax({
                type: "GET",
                url: "http://122.51.249.55:8083/api/article",
                data: q,
                success: function(res) {
                    if (res.code !== 200) {
                        return layer.msg('删除文章失败！')
                    }
                    layer.msg('删除文章成功！')
                        // 当数据删除完成后，需要判断当前这一页中，是否还有剩余的数据
                        // 如果没有剩余的数据了,则让页码值 -1 之后,
                        // 再重新调用 initTable 方法
                        // 4
                    if (len === 1) {
                        // 如果 len 的值等于1，证明删除完毕之后，页面上就没有任何数据了
                        // 页码值最小必须是 1
                        q.pagenum = q.pagenum === 1 ? 1 : q.pagenum - 1
                    }
                    q.act = 'getlist'
                    initArticleList()
                }
            })
        })
    })
})