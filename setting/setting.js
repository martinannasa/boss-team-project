$(function() {
  var layer = layui.layer;
  var form = layui.form;
  //初始化网站信息
  getSetting();
  getMessage();
  function getSetting() {
    $.ajax({
      url: "http://122.51.249.55:8083/api/webset",
      method: "GET",
      success: function(res) {
        if (res.code !== 200) {
          return layer.msg("获取网站信息失败！");
        }
        console.log(res);
        //调用 from.val() 快速为表单赋值
        form.val("formSet", res.data);
      }
    });
  }
  //点击保存发起post请求
  var apiUrl = "http://122.51.249.55:8083";
  var imgUrl = "";
  // 初始化点击上传
  function getMessage() {
    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/webset",
      success: function(res) {
        var data = res.data;
        form.val("formSet", data);
        imgUrl = res.data.weblogo;
        imgUrl = apiUrl + imgUrl;
        $("#Image").attr("src", imgUrl);
        $("#photoMsg").html(res.data.weblogo);
      }
    });
  }
  var str;
  layui.use("upload", function() {
    var upload = layui.upload;

    var uploadInst = upload.render({
      elem: "#goOn", //上传file
      url: "http://122.51.249.55:8083/api/upload",
      field: "img",
      done: function(res) {
        // console.log(res.data[0].path);

        $("#Image").attr("src", res.data[0].url);
        str = res.data[0].path;
        return layer.msg("上传成功！");
      },
      error: function() {
        //请求异常回调
        return layer.msg("上传失败！");
      }
    });
  });
  console.log(str);
  //保存功能
  $(".layui-form").on("submit", function(e) {
    console.log("1");
    e.preventDefault();
    var data = $(this).serialize() + "&act=edit" + "&weblogo=" + str;
    $.ajax({
      method: "POST",
      url: "http://122.51.249.55:8083/api/webset",

      data: data,

      success: function(res) {
        if (res.code !== 200) {
          // console.log('333');

          return layer.msg("保存信息失败");
        }
        layer.msg("保存成功");
        // console.log('333');
      }
    });
    console.log("22");
  });
  $("#close").on("click", function() {
    $(".boxs").hide(500);
  });
  $("#goOn").on("click", function() {
    $(".boxs").show(500);
  });
});
