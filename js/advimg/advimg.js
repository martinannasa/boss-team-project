$(function () {

    // 调用 layer 
    var layer = layui.layer
    var form = layui.form
    var laypage = layui.laypage
    var upload = layui.upload

    // 定义一个查询参数对象，将来请求数据的时候，
    //  需要将请求参数的对象提交服务器
    var q = {
        act: "getlist",

    }
    initAdvimgList()

    //  获取广告图的数据
    function initAdvimgList() {
        $.ajax({
            type: 'GET',
            url: 'http://122.51.249.55:8083/api/advimg',
            data: q,
            success: function (res) {
                //  console.log(res);
                // 使用模板引擎渲染页面数据
                console.log(res.data[0].src);
                console.log(res.data[0].advimgsrc);
                var htmlStr = template('tpl-table', res)
                $('tbody').html(htmlStr)

            }
        })
    }
    // 广告图列表和添加广告图的点击按钮切换页面
    // 广告图列表按钮
    // 广告图列表
    $('#advimg-list-btn').on('click', function () {
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#advimg-add-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
        $('#advimg-addLaybel').hide();
        $('#advimg-layelList').show();
    })
    //  添加广告图
    $('#advimg-add-btn').on('click', function () {
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#advimg-list-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
        $('#advimg-layelList').hide();
        $('#advimg-addLaybel').show();
    })


    //  创建一个上传图片的实例
    // 初始化富文本编辑器
    // 1. 初始化图片裁剪器
    var $image = $('#image')

    // 2. 裁剪选项
    var options = {
        aspectRatio: 400 / 280,
        preview: '.img-preview'
    }

    // 3. 初始化裁剪区域
    $image.cropper(options)









})