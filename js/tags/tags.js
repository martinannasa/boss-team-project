$(function () {
    // 调用 layer.msg 的弹出层
    var layer = layui.layer
    var form = layui.form
    var laypage = layui.laypage;
    // layui.use('layer', function () {
    //     layer
    //     form
    //     laypage
    // })

    // 定义一个查询参数对象，将来请求数据的时候
    //  需要将请求参数对象提交到服务器
    var q = {
        act: "getlist",
        start: 1,  // 页码值，默认请求第一页
        pagesize: 10, // 每页显示几条数据，默认10条
        cate_id: '',  // 标签分类的 id
        code: '',  //  发布状态
    }

    initLaybelList()
    //  获取标签管理的数据
    function initLaybelList() {
        $.ajax({
            type: 'get',
            url: 'http://122.51.249.55:8083/api/tag',
            data: q,
            success: function (res) {
                //  console.log(res);
                //  使用模板引擎渲染页面的数据
                var htmlStr = template('tpl-table', res)
                $('tbody').html(htmlStr)
                // 调用渲染分页的方法
                renderPage(res.allNum)
            }
        })
    }

    //  点击事件切换标签界面
    $('#addLaybel-btn').on('click', function () {
        $('#article-laybelList').hide();
        $('#article-addLaybel').show();
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#laybelList-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
    })

    $('#laybelList-btn').on('click', function () {
        $('#article-laybelList').show();
        $('#article-addLaybel').hide();
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#addLaybel-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
    })
    //  卡片区域的JS 文件
    //Demo
    // layui.use('form', function () {
    //     var form = layui.form;

    //监听提交
    // form.on('submit(formDemo)', function (data) {
    //     layer.msg(JSON.stringify(data.field));
    //     return false;
    // });
    // });

    //  为add-form 表单添加提交事件
    $('body').on('submit', '#add-form', function (e) {
        e.preventDefault()

        $.ajax({
            type: 'POST',
            url: 'http://122.51.249.55:8083/api/tag',
            data: {
                act: "add",
                tagname: $('#add-input').val(),
            },
            success: function (res) {
                if (res.code !== 200) {
                    layer.msg('新增标签失败');
                }
                initLaybelList()
                layer.msg('新增标签成功');
                // 新增完成后清空输入框
                $('.layui-input').val('')
            }
        })
    })


    // 通过代理的形式为 #made-btn 绑定点击事件
    var indexEdit = null

    $('tbody').on('click', '#made-btn', function () {
        indexEdit = layer.open({
            type: 1,
            area: ['500px', '250px'],
            title: '修改标签信息',
            content: $('#dialog-add').html(),
        })
        //  为修稿标签弹出层的取消按钮绑定点击事件关闭弹出层
        $('body').on('click', '#layer-reset', function () {

            layer.close(layer.index)
        })


        //  通过自定义属性获取每行对应的数据的值
        var id = $(this).attr('data-id')



        $('#form-add input').val($(this).attr('data-tagname'))


        //  修改标签内容
        $('body').on('submit', '#form-add', function (e) {
            e.preventDefault()

            $.ajax({
                type: 'post',
                url: 'http://122.51.249.55:8083/api/tag',
                data: {
                    id: id,
                    act: 'edit',
                    tagname: $('#edit-input').val()
                },

                success: function (res) {
                    if (res.code !== 200) {
                        console.log(res);
                        return layer.msg('修改标签失败')
                    }

                    layer.msg('修改成功')
                    layer.close(indexEdit)
                    initLaybelList()
                    id = '';
                }

            })

        })

    })
    // 通过代理的形式， 为删除按钮绑定点击事件
    $('tbody').on('click', '#del-btn', function () {
        var id = $(this).attr('data-id')
        var tagname = $(this).attr('data-tagname')
        //  提示用户是否要删除
        var deleterStr = '确认删除 ' + tagname + ' 标签吗？'

        layer.confirm(deleterStr, { icon: 3, title: '提示' }, function (index) {
            //  发起删除的 ajax 请求
            $.ajax({
                type: 'GET',
                url: 'http://122.51.249.55:8083/api/tag',
                data: {
                    id: id,
                    act: 'del'
                },
                success: function (res) {
                    if (res.code !== 200) {
                        return layer.msg('删除失败')
                    }
                    layer.msg('删除成功')
                    initLaybelList()
                }
            })

            layer.close(index);
        });

    })



    // 定义渲染分页的方法
    function renderPage(allNum) {
        //  调用 laypage.render() 方法来渲染分页结构
        laypage.render({
            elem: 'pageBox', //注意，这里的 test1 是 ID，不用加 # 号
            count: allNum,  //  数据总数
            limit: q.pagesize,  // 每页条数
            curr: q.start, //  设置被默认选中的分页
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [5, 10, 11, 13, 15],
            //  分页发生切换时，触发 jump 回调

            //  触发 jump回调的两种方式
            // 1，点击页码时触发jump回调
            // 2，只要调用了laypage.render 就会触发jump回调
            jump: function (obj, first) {
                q.start = obj.curr
                //  console.log(obj.curr);
                //  把最新的页码值赋值到 q 这个查询参数对象中
                q.pagesize = obj.limit
                if (!first) {
                    initLaybelList()
                }
            }
        })

    }

})
