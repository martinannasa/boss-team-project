$(function() {
  var myChart = echarts.init(document.querySelector(".main-echarts"));
  (function() {
    option = {
      tooltip: {
        trigger: "item"
        // formatter: '{a} <br/>{b}: {c} ({d}%)'
      },
      legend: {
        data: [">100", "50-100", "10-50", "<10"]
      },
      series: [
        {
          name: "访问来源",
          type: "pie",
          selectedMode: "single",
          radius: [0, "30%"],
          label: {
            position: "inner",
            fontSize: 12
          },
          labelLine: {
            show: false
          },
          data: [
            { value: 700, name: "安卓端" },
            { value: 1600, name: "IOS端" },
            { value: 679, name: "PC端", selected: true }
          ]
        },
        {
          name: "访问来源",
          type: "pie",
          radius: ["45%", "60%"],
          labelLine: {
            length: 30
          },
          label: {
            rich: {
              a: {
                color: "#6E7079",
                lineHeight: 22,
                align: "center"
              },
              hr: {
                borderColor: "#8C8D8E",
                width: "100%",
                borderWidth: 1,
                height: 0
              },
              b: {
                color: "#4C5058",
                fontSize: 14,
                fontWeight: "bold",
                lineHeight: 33
              },
              per: {
                color: "#fff",
                backgroundColor: "#4C5058",
                padding: [3, 4],
                borderRadius: 4
              }
            }
          },
          data: [
            { value: 1048, name: ">100" },
            { value: 335, name: "50-100" },
            { value: 310, name: "10-50" },
            { value: 251, name: "<10" }
          ]
        }
      ]
    };
    myChart.setOption(option);
    window.addEventListener("resize", function() {
      myChart.resize();
    });
  })();

  var myChart2 = echarts.init(document.querySelector(".biao_box"));
  (function() {
    option = {
      title: {
        text: "堆叠区域图"
      },
      tooltip: {
        trigger: "axis",
        axisPointer: {
          type: "cross",
          label: {
            backgroundColor: "#6a7985"
          }
        }
      },
      legend: {
        data: ["请求", "图片", "跟他头问你", "好吃补", "段子", "科技", "时尚", "反反复复", "dfh", "添加"]
      },
      toolbox: {
        feature: {
          saveAsImage: {}
        }
      },
      grid: {
        left: "3%",
        right: "4%",
        bottom: "3%",
        containLabel: true
      },
      xAxis: [
        {
          type: "category",
          boundaryGap: false,
          data: ["10月份","11月份","12月份","1月份","2月份","3月份","4月份","5月份","6月份","7月份","8月份","9月份"]
        }
      ],
      yAxis: [
        {
          type: "value"
        }
      ],
      series: [
        {
          name: "请求",
          type: "line",
          stack: "总量",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [2,0,0,0,0,0,0,1,7,9,12,0]
        },
        {
          name: "图片",
          type: "line",
          stack: "总量",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [0,0,0,0,0,0,0,2,7,13,17,0]
        },
        {
          name: "跟他头问你",
          type: "line",
          stack: "总量",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [0,0,0,0,0,0,0,4,11,15,22,0]
        },
        {
          name: "好吃补",
          type: "line",
          stack: "总量",
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [0,0,0,0,0,0,0,0,7,12,15,0]
        },
        {
            name: "段子",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,1,2,7,11,0]
          },
          {
            name: "科技",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,1,6,8,12,0]
          },
          {
            name: "时尚",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,0,5,6,8,0]
          },
          {
            name: "反反复复",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,0,0,0,0,0]
          },
          {
            name: "dfh",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,0,0,0,0,0]
          },{
            name: "添加",
            type: "line",
            stack: "总量",
            areaStyle: {},
            emphasis: {
              focus: "series"
            },
            data: [0,0,0,0,0,0,0,0,0,0,0,0]
          },
        {
          name: "搜索引擎",
          type: "line",
          stack: "总量",
          label: {
            show: true,
            position: "top"
          },
          areaStyle: {},
          emphasis: {
            focus: "series"
          },
          data: [0,0,0,0,0,0,0,0,7,12,15,0]
        }
      ]
    };
    myChart2.setOption(option);
    window.addEventListener("resize", function() {
      myChart2.resize();
    });
  })();

  var arr1 = [];
  var arr2 = [];
  getData();
  function getData() {
    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/article",
      data: {
        act: "getlist"
      },
      success: function(res) {
        var articleNum = res.allNum;
        $("#articleNum").html(articleNum + "篇");
      }
    });

    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/advpos",
      data: {
        act: "getlist"
      },
      success: function(res) {
        var advNum = res.allNum;
        $("#advNum").html("广告位" + advNum + "个");
      }
    });

    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/advimg",
      data: {
        act: "getlist"
      },
      success: function(res) {
        var advPhp = res.allNum;
        $("#advPhp").html("广告图" + advPhp + "张");
      }
    });

    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/menu",
      data: {
        act: "getlist"
      },
      success: function(res) {
        for (var i = 0; i < res.data.length; i++) {
          if (res.data[i].num !== 0) {
            arr1.push(res.data[i].catename);
            arr2.push(res.data[i].num);
          }
        }
        var myChart1 = echarts.init(document.querySelector(".main-echarts2"));
        (function() {
          option = {
            tooltip: {
              trigger: "axis",
              axisPointer: {
                // 坐标轴指示器，坐标轴触发有效
                type: "shadow" // 默认为直线，可选为：'line' | 'shadow'
              }
            },
            grid: {
              left: "3%",
              right: "4%",
              bottom: "3%",
              containLabel: true
            },
            xAxis: [
              {
                type: "category",
                data: arr1,
                axisTick: {
                  alignWithLabel: true
                }
              }
            ],
            yAxis: [
              {
                type: "value"
              }
            ],
            series: [
              {
                name: "直接访问",
                type: "bar",
                barWidth: "60%",
                data: arr2
              }
            ]
          };
          myChart1.setOption(option);
          window.addEventListener("resize", function() {
            myChart1.resize();
          });
        })();
      }
    });
  }
});
