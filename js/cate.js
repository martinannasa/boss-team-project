$(function() {
  $(".layui-tab-title").on("click", "li", function() {
    $(this)
      .addClass("layui-bg-red")
      .siblings()
      .removeClass("layui-bg-red");
  });

  var q = {
    pagenum: 1,
    pagesize: 8
  };

  getClassification();
  //获取分类
  function getClassification() {
    $.ajax({
      method: "GET",
      url: "http://122.51.249.55:8083/api/menu",
      data: {
        act: "getlist",
        start: q.pagenum
        //   pagesize:q.pagesize
      },
      success: function(res) {
        // console.log(res);
        var htmlStr = template("mo-table", res);
        $("tbody").html(htmlStr);
        renderPage(res.allNum);
      }
    });
  }
  var layer = layui.layer;
  //   添加分类
  $(".layui-form").on("submit", function(e) {
    e.preventDefault();
    var value = $(".addClassification").val();
    $.ajax({
      method: "POST",
      url: "http://122.51.249.55:8083/api/menu",
      data: {
        act: "add",
        catename: value
      },
      success: function(res) {
        if (res.code !== 200) return layer.msg("新增分类失败");

        getClassification();
        layer.msg("新增分类成功");
        $(".layui-input").val("");
      }
    });
  });

  var data = {
    act: "edit"
  };
  var indexEdit = null;
  //  编辑分类
  $("tbody").on("click", ".edit", function() {
    indexEdit = layer.open({
      type: 1,
      area: ["650px", "250px"],
      title: "修改分类信息",
      content: $("#dialog-change").html()
    });
    $("body").on('click','#excit',function(){
        layui.layer.close(indexEdit);
    })
    data.id = $(this)
      .parent()
      .siblings(".data_id")
      .html();
  });

  $("body").on("submit", ".change-from", function(e) {
    e.preventDefault();
    data.catename = $(".change_content").val();
    $.ajax({
      method: "POST",
      url: "http://122.51.249.55:8083/api/menu",
      data: data,
      success: function(res) {
        if (res.code !== 200) return layer.msg("更新数据失败！");

        layer.msg("更新数据成功！");
        layer.close(indexEdit);
        getClassification();
      }
    });

    
  });

  //   删除分类
  $("tbody").on("click", ".delete", function() {
    var nowId = $(this)
      .parent()
      .siblings(".data_id")
      .html();
    console.log(nowId);
    layer.confirm("确定删除该分类？ ", { icon: 3, title: "提示" }, function(
      index
    ) {
      $.ajax({
        method: "GET",
        url: "http://122.51.249.55:8083/api/menu",
        data: {
          id: nowId,
          act: "del"
        },
        success: function(res) {
          if (res.code !== 200) return layer.msg("删除失败");

          layer.msg("删除成功");
          layer.close(index);
          getClassification();
        }
      });
    });
  });

  var laypage = layui.laypage;
  //   设置分页
  function renderPage(allNum) {
    laypage.render({
      elem: "pageBox",
      count: allNum,
      limit: 10,
      curr: q.pagenum,
      layout:['count','prev','page','next','skip'],
      jump: function(obj, first) {
        q.pagenum = obj.curr;
        if (!first) {
          getClassification();
        }
      }
    });
  }
});
