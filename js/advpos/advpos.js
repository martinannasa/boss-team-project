$(function () {

    // 调用 layer 
    var layer = layui.layer
    var form = layui.form
    var laypage = layui.laypage;

    // 定义一个查询参数对象，将来请求数据的时候，
    //  需要将请求参数的对象提交服务器
    var q = {
        act: "getlist",
        start: 1,
        pagesize: 10,
        cate_id: '',
        code: '',

    }

    //  调用获取广告位数据接口
    initAdvposList()
    //  获取广告位的数据
    function initAdvposList() {
        $.ajax({
            type: 'GET',
            url: 'http://122.51.249.55:8083/api/advpos',
            data: q,
            success: function (res) {
                // 使用模板引擎渲染页面数据
                var htmlStr = template('tpl-table', res)
                $('tbody').html(htmlStr)
                renderPage(res.allNum)
            }
        })
    }

    // 广告位列表和添加广告位的点击按钮切换页面
    // 广告位列表按钮
    $('#advpos-list-btn').on('click', function () {
        //  颜色的变化
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#advpos-add-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
        // 页面显示和隐藏
        $('#advpos-addLaybel').hide();
        $('#advpos-laybelList').show();
    })
    // 添加广告位按钮
    $('#advpos-add-btn').on('click', function () {
        $(this).addClass('layui-btn-danger').removeClass('layui-btn-primary')
        $('#advpos-list-btn').addClass('layui-btn-primary').removeClass('layui-btn-danger')
        $('#advpos-addLaybel').show();
        $('#advpos-laybelList').hide();
    })

    // 为 advpos-add-form 表单添加提交事件
    $('body').on('submit', '#advpos-add-form', function (e) {
        e.preventDefault()  //  阻止默认行为
        $.ajax({
            type: 'POST',
            url: 'http://122.51.249.55:8083/api/advpos',
            data: {
                act: 'add',
                advposname: $('#advpos-name').val(),
                advposdesc: $('#advpos-discript').val(),
                advpossize: $('#advpos-size').val()
            },
            success: function (res) {
                // console.log(res);
                if (res.code !== 200) {
                    return layer.msg('添加失败')
                }
                layer.msg('添加成功')
                //  添加数据成功后重新获取一次页面数据
                initAdvposList()
                $('.layui-input').val('')
            }
        })
    })
    //  定义一个正则表达式
    form.verify({
        advposSize: [/^(\d+),(\d+)$/, '广告提尺寸，宽高之间注意英文逗号间隔']
    })

    // 为修改广告信息按钮添加点击事件
    var indexEdit = null
    $('tbody').on('click', '#made-btn', function () {
        indexEdit = layer.open({
            type: 1,
            area: ['700px', '400px'],
            title: '修改广告位信息',
            content: $('#devpos-edit-text').html(),
        })
        //  为修稿标签弹出层的取消按钮绑定点击事件关闭弹出层
        $('body').on('click', '#layer-reset', function () {

            layer.close(layer.index)
        })

        //  通过自定义属性获取每行对应的数据的值
        var id = $(this).attr('data-id')

        $('#form-add #edit-advpos-name').val($(this).attr('data-advposname'))
        $('#form-add #edit-advpos-descript').val($(this).attr('data-advposdesc'))
        $('#form-add #edit-advpos-size').val($(this).attr('data-advpossize'))


        //  通过代理 将修改页面的弹出层上的数据通过ajax上传到服务器修改数据
        $('body').on('submit', '#form-add', function (e) {
            e.preventDefault()  // 阻止默认行为
            $.ajax({
                type: 'post',
                url: 'http://122.51.249.55:8083/api/advpos',
                data: {
                    id: id,
                    act: 'edit',
                    advposname: $('#edit-advpos-name').val(),
                    advposdesc: $('#edit-advpos-descript').val(),
                    advpossize: $('#edit-advpos-size').val(),
                },
                success: function (res) {
                    //  console.log(res);
                    if (res.code !== 200) {
                        return layer.msg('修改广告位信息失败')
                    }
                    layer.msg('修改广告位信息成功')
                    layer.close(indexEdit) //  更新完成后关闭弹出层
                    initAdvposList() // 重新渲染页面
                }
            })
        })
    })


    //  通过代理删除数据 将修改页面的弹出层上的数据通过ajax上传到服务器
    $('tbody').on('click', '#del-btn', function () {
        var id = $(this).attr('data-id')

        var advposname = $(this).attr('data-advposname')
        var deleterStr = '确认删除 ' + advposname + ' 广告位吗？'
        // 通过layui 渲染出删除的弹出层
        layer.confirm(deleterStr, { icon: 3, title: '提示' }, function (index) {
            //  发起删除的 ajax 请求
            $.ajax({
                type: 'GET',
                url: 'http://122.51.249.55:8083/api/advpos',
                data: {
                    id: id,
                    act: 'del'
                },
                success: function (res) {
                    //   console.log(res);
                    if (res.code !== 200) {
                        return layer.msg('删除失败')
                    }
                    layer.msg('删除成功')

                    initAdvposList()
                }
            })

            layer.close(index);
        });

    })



    // 定义渲染分页的方法
    function renderPage(allNum) {
        //  调用 laypage.render() 方法来渲染分页结构
        laypage.render({
            elem: 'pageBox', //注意，这里的 test1 是 ID，不用加 # 号
            count: allNum,  //  数据总数
            limit: q.pagesize,  // 每页条数
            curr: q.start, //  设置被默认选中的分页
            layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
            limits: [5, 10, 11, 13, 15],
            //  分页发生切换时，触发 jump 回调

            //  触发 jump回调的两种方式
            // 1，点击页码时触发jump回调
            // 2，只要调用了laypage.render 就会触发jump回调
            jump: function (obj, first) {
                q.start = obj.curr
                //  console.log(obj.curr);
                //  把最新的页码值赋值到 q 这个查询参数对象中
                q.pagesize = obj.limit
                if (!first) {
                    initAdvposList()
                }
            }
        })

    }



})

